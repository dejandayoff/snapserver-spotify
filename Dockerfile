FROM ubuntu:16.04 as librespotBuilder
RUN  apt-get update \
  && apt-get install -y \
  cargo \
  libsdl2-dev \
  build-essential \
  libasound2-dev \
  wget
RUN mkdir /build \
  && cd /build \
  && wget https://github.com/librespot-org/librespot/archive/v0.1.1.tar.gz \
  && tar -xf v0.1.1.tar.gz \
  && cd librespot-0.1.1/ \
  && cargo build --release

FROM ubuntu:16.04 as shairportBuilder

ARG  shairport_version=3.3.6

RUN apt-get update \
 && apt-get install -y \
  build-essential git \ 
  xmltoman \ 
  autoconf \ 
  automake \ 
  libtool \ 
  libdaemon-dev \ 
  libpopt-dev \ 
  libconfig-dev \ 
  libasound2-dev \ 
  libpulse-dev \ 
  avahi-daemon \ 
  libavahi-client-dev \ 
  libssl-dev \ 
  libsoxr-dev \ 
  libsndfile1-dev \
  libmosquitto-dev \ 
  wget 

RUN wget https://github.com/mikebrady/shairport-sync/archive/${shairport_version}.tar.gz \
  && tar -xf ${shairport_version}.tar.gz \
  && cd shairport-sync-${shairport_version} \
  && autoreconf -fi \
  &&./configure --sysconfdir=/etc --with-configfiles \
    --with-alsa \
    --with-avahi \
    --with-ssl=openssl \
    --with-systemd \
    --with-metadata \
    --with-mqtt-client \
    --with-pipe \
    --with-pa \
    --with-stdout \
    --with-soxr\
  && make

FROM ubuntu:16.04

ARG  snapcast_version=0.21.0
ARG  shairport_version=3.3.6
ENV  STREAM_NAME Snapserver

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# get the stuffs
RUN apt-get -qq update &&\
    apt-get -qq install build-essential git avahi-daemon avahi-utils dbus \
        supervisor bzip2 portaudio19-dev libvorbisfile3 curl libprotoc-dev \
        cargo libmosquitto-dev  libpulse-dev libsoxr-dev libconfig-dev mosquitto-clients

# Snapcast, Shairport-sync,avahi and dbus
RUN curl -L -o /root/out.deb "https://github.com/badaix/snapcast/releases/download/v${snapcast_version}/snapserver_${snapcast_version}-1_amd64.deb" &&\
    dpkg -i --force-all /root/out.deb &&\
    apt-get -y -f install &&\
    mkdir -p /root/.config/snapcast/ &&\
    rm -rf /var/run/* &&\
    mkdir -p /var/run/dbus &&\
    chown messagebus:messagebus /var/run/dbus &&\
    dbus-uuidgen --ensure

# cleanup
RUN apt-get -qq autoremove &&\
    apt-get -qq clean &&\
    rm /root/out.deb &&\
    rm -rf /var/lib/apt/lists/*

# mounting dbus on host so avahi can work.
VOLUME /var/run/dbus

# config-files
ADD ./supervisord.conf /etc/supervisord.conf
ADD ./asound.conf /etc/asound.conf
ADD ./start.sh /start.sh
ADD ./mqtt_start.sh /mqtt_start.sh
ADD ./mqtt_stop.sh /mqtt_stop.sh
COPY --from=librespotBuilder /build/librespot-0.1.1/target/release/librespot /usr/local/bin/
COPY --from=shairportBuilder /shairport-sync-${shairport_version}/shairport-sync /usr/bin/shairport-sync
COPY --from=shairportBuilder /shairport-sync-${shairport_version}/scripts/shairport-sync.conf /etc/shairport-sync.conf
RUN chmod a+x /start.sh

# Snapcast Ports
EXPOSE 1704-1704

# AirPlay ports.
EXPOSE 3689/tcp
EXPOSE 5000-5005/tcp
EXPOSE 6000-6005/udp

# Avahi port
EXPOSE 5353

ENTRYPOINT ["/start.sh"]